package gov.saip.ipportalservice.exception;

import gov.saip.ipportalservice.common.dto.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class RestExceptionHandler {
    private final MessageSource messageSource;

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleApiRequestException(BusinessException businessException) {
        //
        System.out.println("ssss");
        String message = messageSource.getMessage(businessException.getMessage(),
                null, new Locale(""));
        //
        System.out.println("ssss11");

        ApiException apiException = new ApiException(
                message,
                businessException.getHttpStatus(),
                ZonedDateTime.now(ZoneId.of("Z")).toString()
        );
        //
        return new ResponseEntity<>(ApiResponse.builder().error(apiException).success(false)
                .code(businessException.getHttpStatus().value()).build(), businessException.getHttpStatus());
    }

//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
//        System.out.println("XXXXXXXXXXXXXXXXX");
//        String fieldError = ex.getBindingResult().getFieldError().getField();
//        String messageKey = ex.getBindingResult().getFieldError(fieldError).getDefaultMessage();
//        //
//        String message = messageSource.getMessage(messageKey,
//                null, new Locale(""));
//
//        //
//        ApiException apiException = new ApiException(
//                message,
//                (HttpStatus) ex.getStatusCode(),
//                ZonedDateTime.now(ZoneId.of("Z")).toString()
//        );
//        //
//        return new ResponseEntity<>(ApiResponse.builder().error(apiException).success(false)
//                .code(ex.getStatusCode().value()).build(), ex.getStatusCode());
//    }

}
