package gov.saip.ipportalservice.common.model;

import gov.saip.ipportalservice.base.model.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "user_types")
@Setter
@Getter
public class UserType extends BaseEntity<Integer> {

    @NotEmpty
    private String code;

    @NotEmpty
    private String nameEn;

    @NotEmpty
    private String nameAr;

    @OneToMany(mappedBy = "userType", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<User> user;
}
