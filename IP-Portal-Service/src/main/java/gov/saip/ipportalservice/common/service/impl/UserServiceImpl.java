package gov.saip.ipportalservice.common.service.impl;

import gov.saip.ipportalservice.base.service.BaseServiceImpl;
import gov.saip.ipportalservice.common.dto.UserDto;
import gov.saip.ipportalservice.common.mapper.UserMapper;
import gov.saip.ipportalservice.common.model.User;
import gov.saip.ipportalservice.common.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {
    private final UserMapper userMapper;

    @Override
    public User insert(User entity) {
        return super.insert(entity);
    }

    @Override
    public UserDto addUser(UserDto dto) {
      User user =  userMapper.unMap(dto);
      user = insert(user);
      return userMapper.map(user);
    }
}
