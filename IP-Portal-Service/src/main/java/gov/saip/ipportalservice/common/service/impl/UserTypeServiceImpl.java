package gov.saip.ipportalservice.common.service.impl;

import gov.saip.ipportalservice.base.service.BaseServiceImpl;
import gov.saip.ipportalservice.common.model.UserType;
import gov.saip.ipportalservice.common.service.UserTypeService;
import org.springframework.stereotype.Service;

@Service
public class UserTypeServiceImpl extends BaseServiceImpl<UserType, Integer> implements UserTypeService {


}
