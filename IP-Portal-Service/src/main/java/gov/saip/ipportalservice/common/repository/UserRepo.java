package gov.saip.ipportalservice.common.repository;


import gov.saip.ipportalservice.base.repository.BaseRepository;
import gov.saip.ipportalservice.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends BaseRepository<User, Long> {
}
