package gov.saip.ipportalservice.common.model;

import javax.persistence.Entity;
import gov.saip.ipportalservice.base.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Setter
@Getter
public class User extends BaseEntity<Long> {

    @Column(name = "name")
    private String name;

    @Column(name = "national_id")
    private Integer nationalId;

    @ManyToOne()
    @JoinColumn(name="user_type")
    private UserType userType;

}
