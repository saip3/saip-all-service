package gov.saip.ipportalservice.common.mapper;


import gov.saip.ipportalservice.base.mapper.BaseMapper;
import gov.saip.ipportalservice.common.dto.UserDto;
import gov.saip.ipportalservice.common.dto.UserRequestDto;
import gov.saip.ipportalservice.common.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper()
public interface UserMapper extends BaseMapper<User, UserDto> {

    @Override
    @Mapping(source = "name", target = "userName")
    UserDto map(User user);

    @Mapping(source = "name", target = "userName")
    UserRequestDto mapToRequest(User user);

    @Mapping(source = "userName", target = "name")
    User unMapToRequest (UserRequestDto dto);
}
