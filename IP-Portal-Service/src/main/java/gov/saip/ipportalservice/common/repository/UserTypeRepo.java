package gov.saip.ipportalservice.common.repository;


import gov.saip.ipportalservice.base.repository.BaseRepository;
import gov.saip.ipportalservice.common.model.User;
import gov.saip.ipportalservice.common.model.UserType;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepo extends BaseRepository<UserType, Integer> {
}
