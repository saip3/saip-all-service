package gov.saip.ipportalservice.common.service;

import gov.saip.ipportalservice.base.service.BaseService;
import gov.saip.ipportalservice.common.dto.UserDto;
import gov.saip.ipportalservice.common.mapper.UserMapper;
import gov.saip.ipportalservice.common.model.User;
import gov.saip.ipportalservice.common.dto.UserRequestDto;

import java.util.List;

public interface UserService extends BaseService<User, Long> {

    UserDto addUser (UserDto dto);

}
