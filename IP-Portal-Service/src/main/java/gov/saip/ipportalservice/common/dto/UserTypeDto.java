package gov.saip.ipportalservice.common.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;

@Data
public class UserTypeDto {

    private Integer id;
    @NotEmpty
    private String code;

    @NotEmpty
    private String name;

}
