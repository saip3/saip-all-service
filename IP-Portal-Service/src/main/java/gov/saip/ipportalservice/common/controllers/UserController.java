package gov.saip.ipportalservice.common.controllers;

import gov.saip.ipportalservice.common.dto.UserDto;
import gov.saip.ipportalservice.common.mapper.UserMapper;
import gov.saip.ipportalservice.common.model.User;
import gov.saip.ipportalservice.common.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static gov.saip.ipportalservice.util.Constants.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping("")
    @Operation(summary = "Find All", description = "to retrieve list of all data")
    public ResponseEntity<?> findAll() {
        List<UserDto> dtos = userMapper.map(userService.findAll());
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/page")
    @Operation(summary = "Find All Pageable", description = "parameters is  1-pageNum int, optional, default value is 0"
            + ", 2-size int, optional, default value is 10 , 3-sortableColumn string, optional, default value is nameAr")
    public ResponseEntity findAllPaging(
              @RequestParam(required = false, defaultValue = DEFAULT_PAGE_NUMBER) Integer pageNum
            , @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) Integer size
            , @RequestParam(required = false, defaultValue = DEFAULT_SORT_COLUMN) String sortableColumn) {
        Pageable pageable = PageRequest.of(pageNum, size, Sort.by(sortableColumn).descending());
        Page<UserDto> dtos = userService.findAll(pageable).map(t -> userMapper.map(t));
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Find by id", description = "to retrieve element by id")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(userMapper.map(userService.findById(id)));
    }

    @PostMapping("")
    @Operation(summary = "Insert", description = "to adding one element by json object")
    public ResponseEntity<?> insert(@Valid @RequestBody UserDto dto) {
//        User entity = userMapper.unMap(dto);
        UserDto result = userService.addUser(dto);
//        UserDto dtos = userMapper.map(result);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/all")
    @Operation(summary = "Insert All", description = "to adding multi elements by array of json objects", hidden = true)
    public ResponseEntity<?> insertAll(@Valid @RequestBody List<UserDto> dtos) {
        List<User> entities = userMapper.unMap(dtos);
        return ResponseEntity.ok(userService.saveAll(entities));
    }

    @PutMapping("")
    @Operation(summary = "Update", description = "to updating one element by json object")
    public ResponseEntity<?> update(@Valid @RequestBody UserDto dto) {
        User current = userService.getReferenceById(dto.getId());
        User entity  = userMapper.unMap(current, dto);
        User result  = userService.update(entity);
//        UserDto dtos = userMapper.map(result);
        return ResponseEntity.ok(null);
    }

    @PutMapping("/all")
    @Operation(summary = "Update All", description = "to updating multi elements by array of json objects", hidden = true)
    public ResponseEntity<?> updateAll(@Valid @RequestBody List<UserDto> dtos) {
        List<User> entities = userMapper.unMap(dtos);
        return ResponseEntity.ok(userService.saveAll(entities));
    }

    @DeleteMapping("")
    @Operation(summary = "Delete", description = "to delete one element by id")
    public ResponseEntity deleteById(@RequestParam Long id) {
        userService.deleteById(id);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("all")
    @Operation(summary = "Delete All", description = "to delete array of elements by ids", hidden = true)
    public ResponseEntity deleteAll(@RequestParam List<Long> ids) {
        userService.deleteAll(ids);
        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/soft-delete")
    @Operation(summary = "Soft delete")
    public ResponseEntity updateIsDeleted(@RequestParam Long id, @RequestParam boolean isDeleted) {
        return ResponseEntity.ok(userService.updateIsDeleted(id, isDeleted));
    }

}
