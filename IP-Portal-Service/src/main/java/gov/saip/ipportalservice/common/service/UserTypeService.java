package gov.saip.ipportalservice.common.service;

import gov.saip.ipportalservice.base.service.BaseService;
import gov.saip.ipportalservice.common.model.UserType;

public interface UserTypeService extends BaseService<UserType, Integer> {

}
