package gov.saip.ipportalservice.common.dto;

import gov.saip.ipportalservice.util.Constants;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UserRequestDto {
    @NotNull(message = Constants.ErrorKeys.USER_NAME_REQUIRED)
    @NotBlank(message = Constants.ErrorKeys.USER_NAME_REQUIRED)
    private String userName;

    private String password;

    private String email;

}
