package gov.saip.ipportalservice.util;

public class Constants {
    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "20";
    public static final String DEFAULT_SORT_COLUMN = "id";

    public static final class ErrorKeys {
        public static final String PENALTY_NAME_REQUIRED = "PENALTY_NAME_REQUIRED";
        public static final String USER_NAME_REQUIRED = "USER_NAME_REQUIRED";

    }

    public static final class SuccessMessages {
        public static final String PENALTIES_ADDED = "Successfully Saved All Penalties";
    }

}
