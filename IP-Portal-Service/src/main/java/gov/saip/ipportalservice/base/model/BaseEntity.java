package gov.saip.ipportalservice.base.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 *
 * @author nourshaheen
 *
 * @param <ID>
 */
@Setter
@Getter
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> extends AuditTrail implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3855054033844070951L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Schema(description = "The database generated ID", required = true)
	private ID id;

	private int isDeleted;


	public BaseEntity() {
		super();
	}
	public BaseEntity(ID id) {
		this.id = id;
	}


}
