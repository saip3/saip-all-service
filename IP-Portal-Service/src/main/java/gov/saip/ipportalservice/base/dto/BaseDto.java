package gov.saip.ipportalservice.base.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseDto {

    private Long id;
    private String name;

}
