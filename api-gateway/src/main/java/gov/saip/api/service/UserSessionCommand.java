package gov.saip.api.service;

import gov.saip.api.dto.LogoutObjectDto;

public interface UserSessionCommand {

    void logout(LogoutObjectDto logoutObjectDto);

    void send(String serviceUrL, LogoutObjectDto logoutObjectDto);
}
