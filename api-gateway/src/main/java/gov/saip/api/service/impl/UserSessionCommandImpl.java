package gov.saip.api.service.impl;


import gov.saip.api.dao.DenialTokenRepository;
import gov.saip.api.dto.LogoutObjectDto;
import gov.saip.api.mapper.MapperToken;
import gov.saip.api.model.DenialToken;
import gov.saip.api.service.UserSessionCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserSessionCommandImpl implements UserSessionCommand {

    private final DenialTokenRepository denialTokenRepository;
    private final MapperToken mapperToken;

    @Value("${endpoint.session.keycloak.logout-url}")
    private String logoutUrl;

    public void logout(LogoutObjectDto logoutObjectDto){
        denialTokenRepository.findAll().forEach(denialToken -> {
            if (denialToken.getExp() < ((int) new Date().getTime()/1000))
                denialTokenRepository.delete(denialToken);
        });

        DenialToken denialToken = mapperToken.fromJwtToDenialToken(logoutObjectDto.getToken());
        log.info("Invalidate jti {} for user {}", denialToken.getJti(), denialToken.getPreferredUsername());

        send(logoutUrl, logoutObjectDto);
        denialTokenRepository.save(denialToken);
    }


    public void send(String serviceUrL, LogoutObjectDto logoutObjectDto){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Authorization", "Bearer " + logoutObjectDto.getToken());

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("token", logoutObjectDto.getToken());
        body.add("refresh_token", logoutObjectDto.getRefreshToken());
        body.add("client_id", logoutObjectDto.getClientId());

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(body, headers);

        restTemplate.postForObject(serviceUrL, entity, String.class);
    }
}
