package gov.saip.api.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("DenialToken")
@Builder
@Data
public class DenialToken {
    @Id
    @Indexed
    private String jti;
    private int exp;
    private String preferredUsername;
}
