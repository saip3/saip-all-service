package gov.saip.api.dto;

import lombok.Data;

@Data
public class LogoutObjectDto {
    private String token;
    private String refreshToken;
    private String clientId;
}
