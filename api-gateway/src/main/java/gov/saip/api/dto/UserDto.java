package gov.saip.api.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	
	private Long id;
	private String fullName;
	private String loginId;
	private String password;
	
	private List<RoleDto> roles = new ArrayList<>();
	
	

}
