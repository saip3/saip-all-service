package gov.saip.api.dao;

import gov.saip.api.model.DenialToken;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DenialTokenRepository extends CrudRepository<DenialToken, String> {
    Optional<DenialToken> findByJtiEquals(String jti);
    List<DenialToken> findAllByExpIsLessThan(int exp);
}
