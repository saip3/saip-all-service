package gov.saip.api.filter;


import gov.saip.api.dao.DenialTokenRepository;
import gov.saip.api.mapper.MapperToken;
import gov.saip.api.model.DenialToken;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


@Component
@Slf4j
@RequiredArgsConstructor
public class AccessTokenFilter implements GlobalFilter {

    private final DenialTokenRepository denialTokenRepository;
    private final MapperToken mapperToken;

    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        log.info("Api Filter ==>> {}", exchange.getRequest().getPath());
        if (exchange.getRequest().getHeaders().get("Authorization") == null)
            return chain.filter(exchange);

        String jwt = exchange.getRequest().getHeaders().get("Authorization").toString().split(" ")[1];

        DenialToken denialToken = mapperToken.fromJwtToDenialToken(jwt);
        if (denialTokenRepository.findByJtiEquals(denialToken.getJti()).isPresent()) {
            log.info("User {} have been logout", denialToken.getPreferredUsername());
            exchange.getResponse().setRawStatusCode(HttpStatus.UNAUTHORIZED.value());
            return exchange.getResponse().setComplete();
        }

        return chain.filter(exchange);
    }
}
