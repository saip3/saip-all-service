package gov.saip.api.mapper;

import gov.saip.api.model.DenialToken;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Base64;


@Component
@Slf4j
public class MapperToken {
    public DenialToken fromJwtToDenialToken(String jwt){
        String [] jwtParts = jwt.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(jwtParts[1]));
        JSONObject jsonObject =  new JSONObject(payload);
        return DenialToken.builder()
                .jti((String) jsonObject.get("jti"))
                .exp((int) jsonObject.get("exp"))
                .preferredUsername((String) jsonObject.get("preferred_username"))
            .build();
    }
}
