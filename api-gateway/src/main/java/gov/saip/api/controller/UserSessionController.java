package gov.saip.api.controller;

import gov.saip.api.dto.LogoutObjectDto;
import gov.saip.api.service.UserSessionCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserSessionController {
    private final UserSessionCommand userSessionCommand;

    @PostMapping("/logout")
    public void logout(@RequestBody LogoutObjectDto logoutObjectDto){
        userSessionCommand.logout(logoutObjectDto);
    }
}
