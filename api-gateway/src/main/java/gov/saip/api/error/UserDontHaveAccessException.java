package gov.saip.api.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class UserDontHaveAccessException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -275295144941607107L;

	public UserDontHaveAccessException(String message) {
        super(message);
    }
}
