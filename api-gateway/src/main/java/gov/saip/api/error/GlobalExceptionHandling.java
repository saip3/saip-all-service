package gov.saip.api.error;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
@ControllerAdvice
public class GlobalExceptionHandling  {

	@ExceptionHandler(BusinessException.class)
	public final ResponseEntity<Object> handleBusinessException(BusinessException ex,
			WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(HttpStatus.EXPECTATION_FAILED, ex.getLocalizedMessage() ,ex.toString(), details);
		return new ResponseEntity<Object>(error, error.getStatus());
	}

	private List<String> asList(String... items) {
		return Arrays.asList(items);
	}

	@ExceptionHandler(UserDontHaveAccessException.class)
	public final ResponseEntity<Object> handleUserDontHaveAccessException(UserDontHaveAccessException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), ex.toString(), details);
		return new ResponseEntity<Object>(error, error.getStatus());
	}

	@ExceptionHandler({ AccessDeniedException.class })
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {	
		
		ErrorResponse respose = new ErrorResponse(HttpStatus.FORBIDDEN,
				ex.getLocalizedMessage(),ex.toString(), ex.getMessage());

		return new ResponseEntity<Object>(respose, new HttpHeaders(), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error("Application error in: [" + ex.getClass().getName() + "]", ex);
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
				ex.getLocalizedMessage(),
				ex.toString(), details);
		return new ResponseEntity<Object>(error, error.getStatus());
	}
}
