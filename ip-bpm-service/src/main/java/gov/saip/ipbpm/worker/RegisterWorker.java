package gov.saip.ipbpm.worker;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RegisterWorker {

    @JobWorker(type = "create_user")
    public void createUser(ActivatedJob job){
        final String approve = (String) job.getVariablesAsMap().get("approve");
        log.info("Create user with message content: {}", approve);
    }

    @JobWorker(type = "mail")
    public void mail(ActivatedJob job){
        final String approve = (String) job.getVariablesAsMap().get("approve");
        log.info("Sending email with message content: {}", approve);
    }
}
