package gov.saip.ipbpm.controller;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ProcessInstanceEvent;
import io.camunda.zeebe.spring.client.lifecycle.ZeebeClientLifecycle;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/process")
@RequiredArgsConstructor
@Slf4j
public class ProcessController {

    private final ZeebeClient zeebeClient;
    private final ZeebeClientLifecycle zeebeClientLifecycle;

    @GetMapping("/start-process")
    public ResponseEntity startProcess (){
        final ProcessInstanceEvent event =  zeebeClient
                .newCreateInstanceCommand()
                .bpmnProcessId("foreign_user_registration_process")
                .latestVersion()
                .variables(Map.of("approved", "true"))
                .send().join();
        log.info("Started instance for processDefinitionKey='{}', bpmnProcessId='{}', version='{}' with processInstanceKey='{}'",
                event.getProcessDefinitionKey(), event.getBpmnProcessId(), event.getVersion(), event.getProcessInstanceKey());
        return ResponseEntity.ok(event);
    }

}
