package gov.saip.ipbpm;

import io.camunda.zeebe.spring.test.ZeebeSpringTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ZeebeSpringTest
class IpBpmServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
